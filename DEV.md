

# Create new Laravel Application
# https://laravel.com/docs/9.x/starter-kits#laravel-jetstream
curl -s https://laravel.build/sow-vilt-adminone-starter | bash

# created db: sow-vilt-adminone-starter

# Install Jetstream & Inertia
# https://jetstream.laravel.com/2.x/installation.html#installing-jetstream
composer require laravel/jetstream

# https://jetstream.laravel.com/2.x/installation.html#or-install-jetstream-with-inertia
php artisan jetstream:install inertia


# Build and start Application
# https://jetstream.laravel.com/2.x/installation.html#finalizing-the-installation
npm install
npm run dev
php artisan migrate


# https://github.com/justboil/admin-one-vue-tailwind/blob/master/.laravel-guide/README.md

# https://github.com/justboil/admin-one-vue-tailwind/blob/master/.laravel-guide/README.md#install

npm i pinia @mdi/js chart.js numeral autoprefixer -D

# Add require('autoprefixer') to PostCSS plugin options in webpack.mix.js
# but there is not webpack.mix.js in either my default Laravel install OR their repo
# so did this:

composer update
npm install
npm run dev

git clone https://github.com/justboil/admin-one-vue-tailwind.git AdminOne

# now can see webpack.mix.js, so added the line to webpack


# created `sow-vilt-adminone-starter` on GitLab
# and then...
git init --initial-branch=main
git remote add origin git@gitlab.com:sipofwater/sow-vilt-adminone-starter.git
git add .
git commit -m "Initial commit"
git push -u origin main


# and then, started going thru the other instructions on the page

# onto Add Pages:


###################################################

## ERROR: 3 WARNINGS in child compilations (Use 'stats.children: true' resp. '--stats-children' for more details)

# To Fix:
# https://stackoverflow.com/questions/72083968/1-warning-in-child-compilations-use-stats-children-true-resp-stats-child

npm install autoprefixer@10.4.5 --save-exact

## ERROR: error: Ziggy error: route '/dashboard' is not in the route list




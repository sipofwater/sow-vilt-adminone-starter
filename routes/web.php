<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');

    Route::get('/error', function () {
        return Inertia::render('Error');
    })->name('error');

    Route::get('/forms', function () {
        return Inertia::render('Forms');
    })->name('forms');

    Route::get('/login', function () {
        return Inertia::render('Login');
    })->name('login');

    Route::get('/profile', function () {
        return Inertia::render('Profile');
    })->name('profile');

    Route::get('/responsive', function () {
        return Inertia::render('Responsive');
    })->name('responsive');

    Route::get('/styles', function () {
        return Inertia::render('Style');
    })->name('styles');

    Route::get('/tables', function () {
        return Inertia::render('Tables');
    })->name('tables');

    Route::get('/ui', function () {
        return Inertia::render('Ui');
    })->name('ui');

});
